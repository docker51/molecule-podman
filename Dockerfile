FROM quay.io/podman/stable
RUN yum update -y && \
    yum install -y \
    curl \
    make \
    gcc\
    git \
    python3 \
    python3-pip \
    python3-devel \
    openssl-devel \
    && yum clean all


RUN python3 -m pip install --upgrade --user setuptools \
    && python3 -m pip install --upgrade --user "molecule[podman]" \
    && python3 -m pip install --upgrade --user "molecule[lint]"

ENV PATH "$PATH:/home/root/.local/bin"
ENTRYPOINT ["molecule"]
CMD ["--version"]
